package universitySportsSchedular;

import java.util.Calendar;
import java.util.Date;
import java.util.EnumSet;

public class BaseSystem {
	public enum REPETITION {
	    NONE, DAILY, WEEKLY, MONTHLY
	}

	public enum DAY_OF_WEEK {
	    MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY
	}

	Date dateStart;
	Date dateStop;
	REPETITION repetitionType = REPETITION.NONE;
	EnumSet<DAY_OF_WEEK> daysOfWeek = EnumSet.noneOf(DAY_OF_WEEK.class);

	public void Schedule(Date dateStart, Date dateStop){
	    if (dateStop != null){
	        if (dateStart.after(dateStop))
	            throw new IllegalArgumentException("Date start after date stop.");

	        this.dateStop = new Date(dateStop.getTime());
	    }

	    this.dateStart = new Date(dateStart.getTime());
	}

	/**
	 * 
	 * @param dateToCheck 
	 * @return true if repetition is expected int the day to check
	 */
	public boolean checkDateValid(Date dateToCheck){
	    if (dateToCheck == null)
	        return false;

	    try {
	        checkDateBetweenLimits(dateToCheck);

	        if (repetitionType != null){
	            switch (repetitionType){
	                case WEEKLY: checkWeekRepetition(dateToCheck); break;
	                case MONTHLY: checkMonthRepetition(dateToCheck); break;
	                default:
	            }
	        }
	    }
	    catch (IllegalArgumentException ex){
	        return false;
	    }

	    return true;
	}

	public void checkDateBetweenLimits(Date dateToCheck) throws IllegalArgumentException{
	    if (dateToCheck.before(dateStart))
	        throw new IllegalArgumentException("Date before limit");

	    if (dateStop != null && dateToCheck.after(dateStop))
	        throw new IllegalArgumentException("Date after limit");
	}

	public void checkMonthRepetition(Date dateToCheck) throws IllegalArgumentException{
	    Calendar calendar = Calendar.getInstance();
	    calendar.setTime(dateToCheck);
	    int dayOfWeekToValidate = calendar.get(Calendar.DAY_OF_MONTH);

	    calendar = Calendar.getInstance();
	    calendar.setTime(dateStart);
	    int dayOfWeek = calendar.get(Calendar.DAY_OF_MONTH);

	    if (dayOfWeek != dayOfWeekToValidate){
	        throw new IllegalArgumentException("Not same day of month.");
	    }
	}

	private void checkWeekRepetition(Date dateToCheck) throws IllegalArgumentException{
	    DAY_OF_WEEK day = evaluateDayOfWeek(dateToCheck);
	    if (!daysOfWeek.contains(day))
	        throw new IllegalArgumentException("No repetition in this day of week.");
	}

	private DAY_OF_WEEK evaluateDayOfWeek(Date date){
	    Calendar calendar = Calendar.getInstance();
	    calendar.setTime(date);
	    int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
	    switch (dayOfWeek){
	        case Calendar.MONDAY: return DAY_OF_WEEK.MONDAY;
	        case Calendar.TUESDAY: return DAY_OF_WEEK.TUESDAY;
	        case Calendar.WEDNESDAY: return DAY_OF_WEEK.WEDNESDAY;
	        case Calendar.THURSDAY: return DAY_OF_WEEK.THURSDAY;
	        case Calendar.FRIDAY: return DAY_OF_WEEK.FRIDAY;
	        case Calendar.SATURDAY: return DAY_OF_WEEK.SATURDAY;
	        case Calendar.SUNDAY: return DAY_OF_WEEK.SUNDAY;
	        default: throw new IllegalArgumentException("Unexpected value.");
	    }
	}

	public Date getDateStop() {
	    return dateStop;
	}

	public Date getDateStart() {
	    return dateStart;
	}

	public void setRepetitionType(REPETITION repetitionType) {
	    this.repetitionType = repetitionType;
	}

	public void addDayOfWeek(DAY_OF_WEEK day) {
	    daysOfWeek.add(day);
	}

	public void removeDayOfWeek(DAY_OF_WEEK day){
	    daysOfWeek.remove(day);
	}
	}

