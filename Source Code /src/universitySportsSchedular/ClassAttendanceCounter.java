package universitySportsSchedular;

import java.util.Date;

import org.junit.Test;

public class ClassAttendanceCounter {
	public class ScheduleTest {

		Date mon = new Date(2014, 0, 5);
		Date tue = new Date(2014, 0, 6);
		Date wed = new Date(2014, 0, 7);
		Date thu = new Date(2014, 0, 8);
		Date fri = new Date(2014, 0, 9);
		Date sat = new Date(2014, 0, 10);
		Date sun = new Date(2014, 0, 11);
		Date firstOf2014 = new Date(2014, 0, 1);
		Date lastOf2014 = new Date(2014, 11, 31);

		Date today = GregorianCalendar.getInstance().getTime();
		Date tomorrow = new Date(today.getTime() + (1000L * 3600 * 24));
		Date nextMonth = new Date(today.getTime() + (1000L * 3600 * 24 * 31));
		Date prevMonth = new Date(today.getTime() - (1000L * 3600 * 24 * 31));
		Schedule scheduleTodayTomorrow = new Schedule(today, tomorrow);

		@Test
		public void testDateStartBeforeDateStop(){
		    try{
		        scheduleTodayTomorrow = new Schedule(tomorrow, today);
		        Assert.fail("Should throw IllegalArgumentException when dateStart after dateStop.");
		    }
		    catch (IllegalArgumentException ex){
		        //OK
		    }
		}

		@Test
		public void testDateStopCanBeNull(){
		    Schedule schedule = new Schedule(today, null);
		}

		@Test
		public void testDefensiveCopy(){
		    long todayTime = today.getTime();
		    today.setTime(1000);
		    Assert.assertEquals(todayTime, scheduleTodayTomorrow.getDateStart().getTime());

		    long tomorrowTime = tomorrow.getTime();
		    tomorrow.setTime(1000);
		    Assert.assertEquals(tomorrowTime, scheduleTodayTomorrow.getDateStop().getTime());
		}

		@Test
		public void testDateValidity(){
		    Assert.assertTrue(scheduleTodayTomorrow.checkDateValid(today));
		    Assert.assertTrue(scheduleTodayTomorrow.checkDateValid(tomorrow));
		    Assert.assertFalse(scheduleTodayTomorrow.checkDateValid(prevMonth));
		    Assert.assertFalse(scheduleTodayTomorrow.checkDateValid(nextMonth));

		    Date inDate = new Date(today.getTime() + 1000L);
		    Assert.assertTrue(scheduleTodayTomorrow.checkDateValid(inDate));
		}

		@Test
		public void testDailyRepetition(){
		    Schedule schedule = new Schedule(today, nextMonth);
		    schedule.setRepetitionType(Schedule.REPETITION.DAILY);
		    Assert.assertTrue(schedule.checkDateValid(today));
		    Assert.assertTrue(schedule.checkDateValid(tomorrow));
		    Assert.assertTrue(schedule.checkDateValid(nextMonth));
		}

		@Test
		public void testMonthRepetition(){
		    Schedule schedule = new Schedule(firstOf2014, lastOf2014);
		    schedule.setRepetitionType(Schedule.REPETITION.MONTHLY);

		    // As the scheduleTodayTomorrow starts on 1/1, then every first day of months should be valid
		    for (int i=1; i<=11; i++){
		        Date date = new Date(2014, i, 1);
		        Assert.assertTrue("Valid date with month " + i + " treated as invalid.", schedule.checkDateValid(date));
		    }

		    // Every other days of every month are invalid
		    for (int i=1; i<=11; i++){
		        Date date = new Date(2014, i, 2);
		        Assert.assertFalse("Invalid date with month " + i + " treated as valid.", schedule.checkDateValid(date));
		    }

		}

		@Test
		public void testWeekRepetitionNoValidDateAsDefault(){
		    Schedule schedule = new Schedule(firstOf2014, lastOf2014);
		    schedule.setRepetitionType(Schedule.REPETITION.WEEKLY);

		    Assert.assertFalse(schedule.checkDateValid(mon));
		    Assert.assertFalse(schedule.checkDateValid(tue));
		    Assert.assertFalse(schedule.checkDateValid(wed));
		    Assert.assertFalse(schedule.checkDateValid(thu));
		    Assert.assertFalse(schedule.checkDateValid(fri));
		    Assert.assertFalse(schedule.checkDateValid(sat));
		    Assert.assertFalse(schedule.checkDateValid(sun));
		}

		@Test
		public void testWeekRepetitionAddMonday(){
		    Schedule schedule = new Schedule(firstOf2014, lastOf2014);
		    schedule.setRepetitionType(Schedule.REPETITION.WEEKLY);
		    schedule.addDayOfWeek(Schedule.DAY_OF_WEEK.MONDAY);

		    Assert.assertTrue(schedule.checkDateValid(mon));

		    Assert.assertFalse(schedule.checkDateValid(tue));
		    Assert.assertFalse(schedule.checkDateValid(wed));
		    Assert.assertFalse(schedule.checkDateValid(thu));
		    Assert.assertFalse(schedule.checkDateValid(fri));
		    Assert.assertFalse(schedule.checkDateValid(sat));
		    Assert.assertFalse(schedule.checkDateValid(sun));
		}

		@Test
		public void testWeekRepetitionAddTuesday(){
		    Schedule schedule = new Schedule(firstOf2014, lastOf2014);
		    schedule.setRepetitionType(Schedule.REPETITION.WEEKLY);
		    schedule.addDayOfWeek(Schedule.DAY_OF_WEEK.TUESDAY);

		    Assert.assertTrue(schedule.checkDateValid(tue));

		    Assert.assertFalse(schedule.checkDateValid(mon));
		    Assert.assertFalse(schedule.checkDateValid(wed));
		    Assert.assertFalse(schedule.checkDateValid(thu));
		    Assert.assertFalse(schedule.checkDateValid(fri));
		    Assert.assertFalse(schedule.checkDateValid(sat));
		    Assert.assertFalse(schedule.checkDateValid(sun));
		}

		@Test
		public void testWeekRepetitionAddWednesday(){
		    Schedule schedule = new Schedule(firstOf2014, lastOf2014);
		    schedule.setRepetitionType(Schedule.REPETITION.WEEKLY);
		    schedule.addDayOfWeek(Schedule.DAY_OF_WEEK.WEDNESDAY);

		    Assert.assertTrue(schedule.checkDateValid(wed));

		    Assert.assertFalse(schedule.checkDateValid(mon));
		    Assert.assertFalse(schedule.checkDateValid(tue));
		    Assert.assertFalse(schedule.checkDateValid(thu));
		    Assert.assertFalse(schedule.checkDateValid(fri));
		    Assert.assertFalse(schedule.checkDateValid(sat));
		    Assert.assertFalse(schedule.checkDateValid(sun));
		}

		@Test
		public void testWeekRepetitionAddThursday(){
		    Schedule schedule = new Schedule(firstOf2014, lastOf2014);
		    schedule.setRepetitionType(Schedule.REPETITION.WEEKLY);
		    schedule.addDayOfWeek(Schedule.DAY_OF_WEEK.THURSDAY);

		    Assert.assertTrue(schedule.checkDateValid(thu));

		    Assert.assertFalse(schedule.checkDateValid(mon));
		    Assert.assertFalse(schedule.checkDateValid(tue));
		    Assert.assertFalse(schedule.checkDateValid(wed));
		    Assert.assertFalse(schedule.checkDateValid(fri));
		    Assert.assertFalse(schedule.checkDateValid(sat));
		    Assert.assertFalse(schedule.checkDateValid(sun));
		}

		@Test
		public void testWeekRepetitionAddFriday(){
		    Schedule schedule = new Schedule(firstOf2014, lastOf2014);
		    schedule.setRepetitionType(Schedule.REPETITION.WEEKLY);
		    schedule.addDayOfWeek(Schedule.DAY_OF_WEEK.FRIDAY);

		    Assert.assertTrue(schedule.checkDateValid(fri));

		    Assert.assertFalse(schedule.checkDateValid(mon));
		    Assert.assertFalse(schedule.checkDateValid(tue));
		    Assert.assertFalse(schedule.checkDateValid(wed));
		    Assert.assertFalse(schedule.checkDateValid(thu));
		    Assert.assertFalse(schedule.checkDateValid(sat));
		    Assert.assertFalse(schedule.checkDateValid(sun));
		}

		@Test
		public void testWeekRepetitionAddSaturday(){
		    Schedule schedule = new Schedule(firstOf2014, lastOf2014);
		    schedule.setRepetitionType(Schedule.REPETITION.WEEKLY);
		    schedule.addDayOfWeek(Schedule.DAY_OF_WEEK.SATURDAY);

		    Assert.assertTrue(schedule.checkDateValid(sat));

		    Assert.assertFalse(schedule.checkDateValid(mon));
		    Assert.assertFalse(schedule.checkDateValid(tue));
		    Assert.assertFalse(schedule.checkDateValid(wed));
		    Assert.assertFalse(schedule.checkDateValid(thu));
		    Assert.assertFalse(schedule.checkDateValid(fri));
		    Assert.assertFalse(schedule.checkDateValid(sun));
		}

		@Test
		public void testWeekRepetitionAddSunday(){
		    Schedule schedule = new Schedule(firstOf2014, lastOf2014);
		    schedule.setRepetitionType(Schedule.REPETITION.WEEKLY);
		    schedule.addDayOfWeek(Schedule.DAY_OF_WEEK.SUNDAY);

		    Assert.assertTrue(schedule.checkDateValid(sun));

		    Assert.assertFalse(schedule.checkDateValid(mon));
		    Assert.assertFalse(schedule.checkDateValid(tue));
		    Assert.assertFalse(schedule.checkDateValid(wed));
		    Assert.assertFalse(schedule.checkDateValid(thu));
		    Assert.assertFalse(schedule.checkDateValid(fri));
		    Assert.assertFalse(schedule.checkDateValid(sat));
		}

		@Test
		public void testWeekRepetitionAddMultipleDays(){
		    Schedule schedule = new Schedule(firstOf2014, lastOf2014);
		    schedule.setRepetitionType(Schedule.REPETITION.WEEKLY);
		    schedule.addDayOfWeek(Schedule.DAY_OF_WEEK.MONDAY);
		    schedule.addDayOfWeek(Schedule.DAY_OF_WEEK.WEDNESDAY);
		    schedule.addDayOfWeek(Schedule.DAY_OF_WEEK.FRIDAY);

		    Assert.assertTrue(schedule.checkDateValid(mon));
		    Assert.assertTrue(schedule.checkDateValid(wed));
		    Assert.assertTrue(schedule.checkDateValid(fri));

		    Assert.assertFalse(schedule.checkDateValid(tue));
		    Assert.assertFalse(schedule.checkDateValid(thu));
		    Assert.assertFalse(schedule.checkDateValid(sat));
		    Assert.assertFalse(schedule.checkDateValid(sun));
		}

		@Test
		public void testWeekRepetitionAddAndRemoveDays(){
		    Schedule schedule = new Schedule(firstOf2014, lastOf2014);
		    schedule.setRepetitionType(Schedule.REPETITION.WEEKLY);
		    schedule.addDayOfWeek(Schedule.DAY_OF_WEEK.MONDAY);
		    schedule.addDayOfWeek(Schedule.DAY_OF_WEEK.WEDNESDAY);
		    schedule.addDayOfWeek(Schedule.DAY_OF_WEEK.FRIDAY);

		    Assert.assertTrue(schedule.checkDateValid(mon));
		    Assert.assertTrue(schedule.checkDateValid(wed));
		    Assert.assertTrue(schedule.checkDateValid(fri));

		    schedule.removeDayOfWeek(Schedule.DAY_OF_WEEK.MONDAY);
		    Assert.assertFalse(schedule.checkDateValid(mon));
		    Assert.assertTrue(schedule.checkDateValid(wed));
		    Assert.assertTrue(schedule.checkDateValid(fri));

		    schedule.removeDayOfWeek(Schedule.DAY_OF_WEEK.WEDNESDAY);
		    Assert.assertFalse(schedule.checkDateValid(mon));
		    Assert.assertFalse(schedule.checkDateValid(wed));
		    Assert.assertTrue(schedule.checkDateValid(fri));

		    schedule.removeDayOfWeek(Schedule.DAY_OF_WEEK.FRIDAY);
		    Assert.assertFalse(schedule.checkDateValid(mon));
		    Assert.assertFalse(schedule.checkDateValid(wed));
		    Assert.assertFalse(schedule.checkDateValid(fri));
		}
}
