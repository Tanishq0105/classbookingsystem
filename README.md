# README #

This Project is Booking and Review Management System

### What is this repository for? ###

* The Project is basically The University Sports Centre (USC) needs software for managing the bookings of group exercise classes made by the students. The centre offers different group exercise classes on both Saturday and Sunday. The classes could be Yoga, Zumba, Aquacise, Box Fit, Body Blitz, etc. Each class can accommodate 4 students at most.
* For either day (Saturday or Sunday), there are 3 classes per day: 1 in the morning, 1 in the afternoon, 1 in the evening. The price of each class is different. The class price for the same exercise will remain the same even if they run at a different time.
* A student who wants to book a class needs to first check the timetable and then select a class on a day. A student can check the timetable by two ways: one is by specifying the date and the other is by specifying the exercise name. Students are allowed to change a booking, provided there are still spaces available for the newly selected class. A student can book as many classes as they want so long as there is no time conflict.
* After each group exercise class, students are able to write a review of the class they have attended and provide a numerical rating of the class ranging from 1 to 5 (1: Very dissatisfied, 2: Dissatisfied, 3: Ok, 4: Satisfied, 5: Very Satisfied). The rating information will be recorded in the system.
After four weeks (four weekends), the software system must print:
1. a report containing the number of students per group exercise class on each day, along with the average rating;
2. a report containing the group exercise which has generated the highest income, counting all the same exercise classes together.

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* How to run tests
* Running Exec Jar

### Contribution guidelines ###

* Writing tests
* Code review
